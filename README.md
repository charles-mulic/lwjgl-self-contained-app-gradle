## LWJGL Self-Contained Application (Windows)

This project aims to be the bare minimum required to get a LWJGL Java application to build as a self-contained application. This means that the end user will not need Java installed on their machine, as your distribution will include its own private runtime.

## Building

Run this custom gradle task:
```
gradle buildDist
```

## Running

After running the buildDist task, the base directory will contain a dist folder, which contains a bundles folder. The bundles folder will contain a Game-1.0.exe installer, as well as a Game folder containg a Game.exe which can be run immediately.

## Requirements

In order to build the .exe, javapackager will require you to download Inno Setup (http://jrsoftware.org/isdl.php). You can attempt to build without this, but you will see this as being the suggested fix in the console. MSI has an additional requirement. See the console output.

## See also

https://gitlab.com/charles-mulic/lwjgl-self-contained-app - a pure Java and batch files implementation, no build tools/dependency management