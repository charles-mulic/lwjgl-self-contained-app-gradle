@echo off

set appclass=com.demo.Main
set outputDirectory=dist
set outputFileName=GameTest
set deploySrcDir=build\libs
set srcfiles=Game-1.0-SNAPSHOT.jar
set name="Game"
set title="Game Demo"

REM creates our native application(s) from our jar file
javapackager -deploy -native -outdir %outputDirectory% -outfile %outputFileName% -srcdir %deploySrcDir% -appclass %appclass% -name %name% -title %title%